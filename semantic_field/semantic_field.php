<?php
/*
Plugin Name: Semantic_field
Plugin URI: http://semen.in.ua
Description: Тестовый плагин
Version: 1.0
Author: Семен Гоголев
Author URI: http://semen.in.ua
Copyright 2015  Семен  (email: semenius5@gmail.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
function my_extra_fields_content( $post )
{
    // URL-ы загруженных изображений будем сохранять в мета-полях
    $background = get_post_meta($post->ID, 'post_background', 1);
    ?>
        <label for="post_background">
        <h4>Изображение услуги\товара</h4>
        <input id="post_background" type="text" size="45" name="post_background" value="<?php echo $background; ?>" />
        <input id="post_background_button" type="button" class="button" value="Загрузить" />
        <br />
        <small>Вставьте URL изображения для фона записи или загрузите его</small>
        </label>
        <!-- Создаем проверочное поле для проверки того, что данные пришли с нашей формы -->
        <input type="hidden" name="extra_field_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />
    <?php
}
// Добавляем мета-блок с нашей формой на странице редактирования записи
function my_add_extra_fields() {
   $screens = array( 'post', 'page' );
	foreach ( $screens as $screen ) {
		add_meta_box(
			'extra_fields',
			'Изображение услуги\товара',
			'my_extra_fields_content',
			$screen
		);
	}
}

// Запускаем вышенаписанный код в действие
if( is_admin() )
    add_action('admin_init', 'my_add_extra_fields', 1);

    function my_add_upload_scripts() {
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_register_script(
            'my-upload-script', // Подключаем JS-код задающий поведение  загрузчика и указывающий, куда вставлять ссылку после загрузки изображения
            '/wp-content/plugins/semantic_field/js/upload.js',
            array(
                'jquery',
                'media-upload',
                'thickbox'
            )
        );
        wp_enqueue_script('my-upload-script');
    }

    // Запускаем функцию подключения загрузчика
    if( is_admin() )
    add_action('admin_print_scripts', 'my_add_upload_scripts');

function my_extra_fields_content_update( $post_id ){

    // Если данные пришли не из нашей формы, ничего не делаем
    if ( !wp_verify_nonce($_POST['extra_field_nonce'], __FILE__) )
            return false;
    // Если это автосохранение, то ничего не делаем
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  )
            return false;
    // Проверяем права пользователя
    if ( !current_user_can('edit_post', $post_id) )
            return false;

    $extra_fields = array(
        'post_background' => $_POST['post_background']
    );

    $extra_fields = array_map('trim', $extra_fields);

    foreach( $extra_fields as $key=>$value ){
            // Обновляем, (или создаем) в случае не пустых значений
            if($value)
                update_post_meta($post_id, $key, $value);
    }
    return $post_id;
}

// Запускаем обработчик формы во время сохранения записи
if( is_admin() )
add_action('save_post', 'my_extra_fields_content_update', 0);


//класс для генерации полей метабокса
class trueMetaBox {
	function __construct($options) {
		$this->options = $options;
		$this->prefix = $this->options['id'] .'_';
		add_action( 'add_meta_boxes', array( &$this, 'create' ) );
		add_action( 'save_post', array( &$this, 'save' ), 1, 2 );
	}
	function create() {
		foreach ($this->options['post'] as $post_type) {
			if (current_user_can( $this->options['cap'])) {
				add_meta_box($this->options['id'], $this->options['name'], array(&$this, 'fill'), $post_type, $this->options['pos'], $this->options['pri']);
			}
		}
	}
	function fill(){
		global $post; $p_i_d = $post->ID;
		wp_nonce_field( $this->options['id'], $this->options['id'].'_wpnonce', false, true );
		?>
		<table class="form-table"><tbody><?php
		foreach ( $this->options['args'] as $param ) {
			if (current_user_can( $param['cap'])) {
			?><tr><?php
				if(!$value = get_post_meta($post->ID, $this->prefix .$param['id'] , true)) $value = $param['std'];
				switch ( $param['type'] ) {
					case 'text':{ ?>
						<th scope="row"><label for="<?php echo $this->prefix .$param['id'] ?>"><?php echo $param['title'] ?></label></th>
						<td>
							<input name="<?php echo $this->prefix .$param['id'] ?>" type="<?php echo $param['type'] ?>" id="<?php echo $this->prefix .$param['id'] ?>" value="<?php echo $value ?>" placeholder="<?php echo $param['placeholder'] ?>" class="regular-text" /><br />
							<span class="description"><?php echo $param['desc'] ?></span>
						</td>
						<?php
						break;
					}
					case 'textarea':{ ?>
						<th scope="row"><label for="<?php echo $this->prefix .$param['id'] ?>"><?php echo $param['title'] ?></label></th>
						<td>
							<textarea name="<?php echo $this->prefix .$param['id'] ?>" type="<?php echo $param['type'] ?>" id="<?php echo $this->prefix .$param['id'] ?>" value="<?php echo $value ?>" placeholder="<?php echo $param['placeholder'] ?>" class="large-text" /><?php echo $value ?></textarea><br />
							<span class="description"><?php echo $param['desc'] ?></span>
						</td>
						<?php
						break;
					}
					case 'checkbox':{ ?>
						<th scope="row"><label for="<?php echo $this->prefix .$param['id'] ?>"><?php echo $param['title'] ?></label></th>
						<td>
							<label for="<?php echo $this->prefix .$param['id'] ?>"><input name="<?php echo $this->prefix .$param['id'] ?>" type="<?php echo $param['type'] ?>" id="<?php echo $this->prefix .$param['id'] ?>"<?php echo ($value=='on') ? ' checked="checked"' : '' ?> />
							<?php echo $param['desc'] ?></label>
						</td>
						<?php
						break;
					}
					case 'select':{ ?>
						<th scope="row"><label for="<?php echo $this->prefix .$param['id'] ?>"><?php echo $param['title'] ?></label></th>
						<td>
							<label for="<?php echo $this->prefix .$param['id'] ?>">
							<select name="<?php echo $this->prefix .$param['id'] ?>" id="<?php echo $this->prefix .$param['id'] ?>"><option>...</option><?php
								foreach($param['args'] as $val=>$name){
									?><option value="<?php echo $val ?>"<?php echo ( $value == $val ) ? ' selected="selected"' : '' ?>><?php echo $name ?></option><?php
								}
							?></select></label><br />
							<span class="description"><?php echo $param['desc'] ?></span>
						</td>
						<?php
						break;
					}
				}
			?></tr><?php
			}
		}
		?></tbody></table><?php
	}
	function save($post_id, $post){
		if ( !wp_verify_nonce( $_POST[ $this->options['id'].'_wpnonce' ], $this->options['id'] ) ) return;
		if ( !current_user_can( 'edit_post', $post_id ) ) return;
		if ( !in_array($post->post_type, $this->options['post'])) return;
		foreach ( $this->options['args'] as $param ) {
			if ( current_user_can( $param['cap'] ) ) {
				if ( isset( $_POST[ $this->prefix . $param['id'] ] ) && trim( $_POST[ $this->prefix . $param['id'] ] ) ) {
					update_post_meta( $post_id, $this->prefix . $param['id'], trim($_POST[ $this->prefix . $param['id'] ]) );
				} else {
					delete_post_meta( $post_id, $this->prefix . $param['id'] );
				}
			}
		}
	}
}

$options = array(
	array( // первый метабокс
		'id'	=>	'meta1', // ID метабокса, а также префикс названия произвольного поля
		'name'	=>	'Product', // заголовок метабокса
		'post'	=>	array('post', 'page'), // типы постов для которых нужно отобразить метабокс
		'pos'	=>	'normal', // расположение, параметр $context функции add_meta_box()
		'pri'	=>	'high', // приоритет, параметр $priority функции add_meta_box()
		'cap'	=>	'edit_posts', // какие права должны быть у пользователя
		'args'	=>	array(
			array(
				'id'			=>	'title', // атрибуты name и id без префикса, например с префиксом будет meta1_field_1
				'title'			=>	'Название услуги\товара', // лейбл поля
				'type'			=>	'text', // тип, в данном случае обычное текстовое поле
				'placeholder'		=>	'Введите название услуги\товара', // атрибут placeholder
				'desc'			=>	'поясниения к полю', // что-то типа пояснения, подписи к полю
				'cap'			=>	'edit_posts'
			),
			array(
				'id'			=>	'description',
				'title'			=>	'Описание услуги\товара',
				'type'			=>	'textarea', // большое текстовое поле
				'placeholder'		=>	'Введите описание услуги\товара',
				'desc'			=>	'поясниения к полю',
				'cap'			=>	'edit_posts'
			),
            array(
				'id'			=>	'price', // атрибуты name и id без префикса, например с префиксом будет meta1_field_1
				'title'			=>	'Цена услуги\товара', // лейбл поля
				'type'			=>	'text', // тип, в данном случае обычное текстовое поле
				'placeholder'		=>	'Введите цену услуги\товара', // атрибут placeholder
				'desc'			=>	'поясниения к полю', // что-то типа пояснения, подписи к полю
				'cap'			=>	'edit_posts'
			),
            array(
				'id'			=>	'currency',
				'title'			=>	'Валюта услуги\товара',
				'type'			=>	'select', // выпадающий список
				'desc'			=>	'поясниения к полю',
				'cap'			=>	'edit_posts',
				'args'			=>	array('ГРН' => 'ГРН', 'USD' => 'USD', 'RUB' => 'RUB' ) // элементы списка задаются через массив args, по типу value=>лейбл
			)

		)
	),

);

foreach ($options as $option) {
	$truemetabox = new trueMetaBox($option);
}

/*************
    Виджет
*************/
class widget_my01 extends WP_Widget {
     public function __construct() {
           parent::__construct(
                 'widgets_my',
                 'Виджет',
                 array( 'description' => 'Виджет семантической верстки mrs')
           );
     }
     public function update( $new_instance, $old_instance ) {
           $instance = array();
           $instance['title'] = strip_tags( $new_instance['title'] );
           return $instance;
     }
     public function form( $instance ) { ?>
           <p>
                перетяните этот виджет в нужный сайбар, и в шаблоне отобразятся данные метабоксов
           </p>


<?php }
     public function widget( $args, $instance ) { ?>
     <?php if(is_single() or is_page()):
         //генерируем схему Product если это запись или страница
     ?>
<!--Указывается схема Product.-->
<span class="h">Вывод в сайдбаре(через виджет):</span>
<div itemscope itemtype="http://schema.org/Product">
<?php $post_id = get_the_ID(); ?>
<?php if(get_post_meta(
            $post_id,
            'meta1_title',
            true
        )): ?>
<!--В поле name указывается наименование товара.-->
  <h1 itemprop="name">
    <?php
        $post_id = get_the_ID();
        echo get_post_meta(
            $post_id,
            'meta1_title',
            true
        );
    ?>
  </h1>
<?php endif; ?>
<?php $post_id = get_the_ID(); ?>
<?php if(get_post_meta(
            $post_id,
            'meta1_description',
            true
        )):?>
<!--В поле description дается описание товара.-->
  <span itemprop="description">
      <?php
        $post_id = get_the_ID();
        echo get_post_meta(
            $post_id,
            'meta1_description',
            true
        );
      ?>
  </span>
<?php endif; ?>
<?php $post_id = get_the_ID(); ?>
<?php if(
            get_post_meta(
                $post_id,
                'post_background',
                true
            )): ?>
<img src="<?php
            $post_id = get_the_ID();
            echo get_post_meta(
                $post_id,
                'post_background',
                true
            );
  ?>" itemprop="image">
<?php endif; ?>

<!--Указывается схема Offer.-->
  <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
<!--В поле price указывается цена товара.-->
<?php $post_id = get_the_ID(); ?>
<?php if(get_post_meta(
                $post_id,
                'meta1_price',
                true
            )): ?>
    <span itemprop="price">
        <?php
            $post_id = get_the_ID();
            echo get_post_meta(
                $post_id,
                'meta1_price',
                true
            );
        ?>
    </span>
<?php endif; ?>
<?php $post_id = get_the_ID(); ?>
<?php if( get_post_meta(
                $post_id,
                'meta1_currency',
                true
            )and get_post_meta(
                $post_id,
                'meta1_currency',
                true
            )!=="..."): ?>
<!--В поле priceCurrency указывается валюта.-->
    <span itemprop="priceCurrency">
        <?php
            $post_id = get_the_ID();
            echo get_post_meta(
                $post_id,
                'meta1_currency',
                true
            );
        ?>
    </span>
<?php endif; ?>
  </div>
</div>

     <?php endif;?>
<?php } }

add_action( 'widgets_init', function(){
     register_widget( 'widget_my01' );
});

/*************
    Шорткод
*************/
function semantic_field_shortcode($atts) {
     // Получаем значения атрибутов
    // если значения не были указаны определяем свои
    $get_id=get_the_ID(); //получение ID текущего поста/страницы
    extract( shortcode_atts( array(
        'id' => $get_id //по умолчанию берем значения из текущего поста
    ),
    $atts
    ));

if (is_numeric($id)):
?>
        <span class="h">Вывод в шорткоде:</span>
        <div itemscope itemtype='http://schema.org/Product'>

<?php $get_id=get_the_ID(); ?>
<?php if(get_post_meta(
            $id,
            'meta1_title',
            true
        )):?>
        <!--В поле name указывается наименование товара.-->
        <h1 itemprop='name'>
        <?php echo get_post_meta(
            $id,
            'meta1_title',
            true
        ); ?>
  </h1>
  <?php endif; ?>
<?php $get_id=get_the_ID(); ?>
<?php if(get_post_meta(
            $id,
            'meta1_description',
            true
        )):?>
<!--В поле description дается описание товара.-->
  <span itemprop='description'>
        <?php echo get_post_meta(
            $id,
            'meta1_description',
            true
        ); ?>
  </span>
  <?php endif; ?>
  <?php $get_id=get_the_ID(); ?>
  <?php if(get_post_meta(
                $id,
                'post_background',
                true
            )):?>
<!--В поле image указывается ссылка на картинку товара.-->
  <img src='<?php echo get_post_meta(
                $id,
                'post_background',
                true
            ); ?>
  ' itemprop='image'>
  <?php endif; ?>
<!--Указывается схема Offer.-->
  <div itemprop='offers' itemscope itemtype='http://schema.org/Offer'>
  <?php $get_id=get_the_ID(); ?>
  <?php if( get_post_meta(
                $id,
                'meta1_price',
                true
            )):?>
<!--В поле price указывается цена товара.-->
    <span itemprop='price'>
           <?php echo get_post_meta(
                $id,
                'meta1_price',
                true
            ); ?>
    </span>
<?php endif; ?>
<?php $get_id=get_the_ID(); ?>
<?php if(get_post_meta(
               $id,
                'meta1_currency',
                true
            ) and get_post_meta(
               $id,
                'meta1_currency',
                true
            )!="..."):?>
<!--В поле priceCurrency указывается валюта.-->
    <span itemprop='priceCurrency'>
            <?php echo get_post_meta(
               $id,
                'meta1_currency',
                true
            );   ?>
    </span>
    <?php endif; ?>
  </div>
</div>

<?php endif;
}
add_shortcode('semantic_shortcode', 'semantic_field_shortcode');
?>